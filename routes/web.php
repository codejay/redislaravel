<?php

use App\Events\UserSignedUp;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	$data = [
		'event' => 'UserSignedUp',
		'data' => [
		    'username' => 'Jyoti Duwal'
		]
	];
	
	event(new UserSignedUp('Username'));


	// Redis::publish('test-channel',  json_encode($data));

    return view('welcome');
});
