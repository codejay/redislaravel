<!DOCTYPE html>
<html lang="en">
    <head></head>
    <body>


      <script src = 'https://cdnjs.cloudflare.com/ajax/libs/vue/0.12.16/vue.min.js'></script>
      <script src = 'https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.min.js'></script>

      <ul>
        <li v-repeat='user: users'>@{{ user }}</li>
      </ul>

      <script>
        var socket = io('http://127.0.0.1:3000/');
        var app = new Vue({
            el: 'body',
            data: {
              users: []
            },
            ready: function() {
                socket.on('test-channel:UserSignedUp', function(data) {
                    this.users.push(data.username);
                    console.log(this.users);
                    console.log(this.message);
                }.bind(this))
            }
        })
      </script>
    </body>
</html>
